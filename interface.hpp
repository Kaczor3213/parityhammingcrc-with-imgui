#pragma once

#ifndef INTERFACE_HPP
#define INTERFACE_HPP

#include "imgui.h"
#include "imgui_impl_win32.h"
#include "imgui_impl_dx12.h"
#include <random>
#include <string>
#include <bitset>
#include <iostream>

class MyApp {
public:
    MyApp();
    void ui();
private:
    /*=====================================================================================*/
    /*General tools BEGIN*/
    struct TextFilters
    {
        static int BinaryNumbersFilter(ImGuiInputTextCallbackData* data)
        {
            if (data->EventChar < 256 && strchr("01", (char)data->EventChar))
                return 0;
            return 1;
        }
    };
    std::random_device device;
    std::mt19937 generator;
    std::uniform_int_distribution<unsigned long long> distribution;
    unsigned long long __generate_random_number(size_t const& min, unsigned long long const& max);
    /*General tools END*/
    /*=====================================================================================*/

    /*=====================================================================================*/
    /*PARITY BEGIN*/
    static unsigned long parity_data_bit_length;
    static char parity_input_data_binary[33];
    static unsigned long parity_input_data_decimal;
    static unsigned long parity_input_data_decimal_with_parity_bit;
    static char parity_input_data_with_parity_bit[34];
    static char parity_input_data_with_errors_binary[34];

    void __parity_header();
    unsigned long __make_odd_parity(const unsigned long& data);
    unsigned long __make_even_parity(const unsigned long& data);
    bool __check_if_even(const unsigned long& data);
    bool __check_if_odd(const unsigned long& data);
    unsigned __count_ones_parity(const unsigned long& data);
    /*PARITY END*/
    /*=====================================================================================*/

    /*=====================================================================================*/
    /*HAMMING BEGIN*/
    void __hamming_header();
    /*HAMMING END*/
    /*=====================================================================================*/


    /*=====================================================================================*/
    /*CRC BEGIN*/
    static unsigned long crc_data_bit_length;
    static char crc_input_data_binary[33];
    static unsigned long crc_input_data_decimal;
    static char crc_input_data_with_errors_binary[33];
    static unsigned crc_input_data_with_errors_decimal;

    static constexpr uint16_t CRC12_POLYNOMIAL = 0x80F;
    static constexpr uint16_t CRC12_MASK = 0xFFF;
    static constexpr uint8_t CRC12_KEY_BIT_LEN = 12;

    static char crc12_hex[10];
    static char crc12_dec[10];
    static char crc12_divisor_bin[CRC12_KEY_BIT_LEN + 1];
    static unsigned long crc12_remainder_dec;
    static char crc12_remainder_bin[CRC12_KEY_BIT_LEN + 1];
    static char crc12_check_bin[CRC12_KEY_BIT_LEN + 1];

    static constexpr uint16_t CRC16_POLYNOMIAL = 0x8005;
    static constexpr uint16_t CRC16_MASK = 0xFFFF;
    static constexpr uint8_t CRC16_KEY_BIT_LEN = 16;

    static char crc16_hex[10];
    static char crc16_dec[10];
    static char crc16_divisor_bin[CRC16_KEY_BIT_LEN + 1];
    static unsigned long crc16_remainder_dec;
    static char crc16_remainder_bin[CRC16_KEY_BIT_LEN + 1];
    static char crc16_check_bin[CRC16_KEY_BIT_LEN + 1];

    static constexpr uint16_t CRC16R_POLYNOMIAL = 0x4003;
    static constexpr uint16_t CRC16R_MASK = 0xFFFF;
    static constexpr uint8_t CRC16R_KEY_BIT_LEN = 16;

    static char crc16r_hex[10];
    static char crc16r_dec[10];
    static char crc16r_divisor_bin[CRC16R_KEY_BIT_LEN + 1];
    static unsigned long crc16r_remainder_dec;
    static char crc16r_remainder_bin[CRC16R_KEY_BIT_LEN + 1];
    static char crc16r_check_bin[CRC16R_KEY_BIT_LEN + 1];

    static constexpr uint32_t CRC32_POLYNOMIAL = 0x04C11DB7;
    static constexpr uint32_t CRC32_MASK = 0xFFFFFFFF;
    static constexpr uint8_t CRC32_KEY_BIT_LEN = 32;

    static char crc32_hex[16];
    static char crc32_dec[16];
    static char crc32_divisor_bin[CRC32_KEY_BIT_LEN + 1];
    static unsigned long crc32_remainder_dec;
    static char crc32_remainder_bin[CRC32_KEY_BIT_LEN + 1];
    static char crc32_check_bin[CRC32_KEY_BIT_LEN + 1];

    void __crc_header();
    void __crc_input_ui(std::string labels_prefix);
    void __crc_crc12_tree();
    void __crc_crc16_tree();
    void __crc_crc16r_tree();
    void __crc_crc32_tree();
    unsigned long __get_reminder_crc_bit_by_bit(unsigned long data, long polynomial, long mask, unsigned key_len);
    unsigned long __check_reminder_crc_bit_by_bit(unsigned long data, long reminder, long polynomial, long mask, unsigned key_len);
    /*CRC END*/
    /*=====================================================================================*/
};

#endif 

