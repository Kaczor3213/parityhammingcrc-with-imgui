﻿#include "interface.hpp"
#include <cmath>
#include <cstdint>
#include <string>
#include <sstream>
#include <iomanip>
#include <boost/dynamic_bitset/dynamic_bitset.hpp>


unsigned long MyApp::parity_data_bit_length = 16;
char MyApp::parity_input_data_binary[33] = "0";
unsigned long MyApp::parity_input_data_decimal = 0;
unsigned long MyApp::parity_input_data_decimal_with_parity_bit = 0;
char MyApp::parity_input_data_with_parity_bit[34] = "0";
char MyApp::parity_input_data_with_errors_binary[34] = "0";




/*=====================================================================================*/
/*PARITY BEGIN*/
void MyApp::__parity_header() {

    if (ImGui::CollapsingHeader("Parity"))
    {
        ImGui::Text("Choose parity mode:");
        static int even_or_odd = 0;
        static int parity_refresh_counter = 0;
        static int parity_clear_errors = 0;
        static int parity_validate_data = 0;
        static ImVec4 parity_result_color(1.0f, 1.0f, 0.0f, 1.0f);

        ImGui::RadioButton("even", &even_or_odd, 0); ImGui::SameLine();
        ImGui::RadioButton("odd", &even_or_odd, 1); ImGui::SameLine();
        if (ImGui::Button("parity refresh")) parity_refresh_counter++;
        if (parity_refresh_counter & 1)
        {
            parity_refresh_counter = 0;
            if (even_or_odd) {
                parity_input_data_decimal_with_parity_bit = __make_even_parity(parity_input_data_decimal);
            }
            else {
                parity_input_data_decimal_with_parity_bit = __make_odd_parity(parity_input_data_decimal);
            }
            std::string _str_bit_set;
            boost::dynamic_bitset<> _bit_set(parity_data_bit_length + 1, parity_input_data_decimal_with_parity_bit);
            boost::to_string(_bit_set, _str_bit_set);
            strcpy(parity_input_data_with_parity_bit, _str_bit_set.c_str());
            strcpy(parity_input_data_with_errors_binary, _str_bit_set.c_str());
        }
        static int parity_generate_data_bits_button_click_counter = 0;
        static int crc_refresh_data_bits_button_click_counter = 0;
        static int crc_refresh_error_bits_button_click_counter = 0;

        ImGui::Text("Input entry data:");

        ImGui::SliderInt("parity data bit length", reinterpret_cast<int*>(&parity_data_bit_length), 9, 32);
        ImGui::InputText("parity binary input", parity_input_data_binary, parity_data_bit_length + 1, ImGuiInputTextFlags_ReadOnly);
        ImGui::SameLine();
        if (ImGui::Button("Generate data bits")) parity_generate_data_bits_button_click_counter++;
        if (parity_generate_data_bits_button_click_counter & 1)
        {
            parity_generate_data_bits_button_click_counter = 0;
            parity_input_data_decimal = __generate_random_number(0, 1ull << parity_data_bit_length);
            std::string _str_bit_set;
            boost::dynamic_bitset<> _bit_set(parity_data_bit_length, parity_input_data_decimal);
            boost::to_string(_bit_set, _str_bit_set);
            strcpy(parity_input_data_binary, _str_bit_set.c_str());

            if (even_or_odd) {
                parity_input_data_decimal_with_parity_bit = __make_even_parity(parity_input_data_decimal);
            }
            else {
                parity_input_data_decimal_with_parity_bit = __make_odd_parity(parity_input_data_decimal);
            }
            _bit_set = boost::dynamic_bitset<>(parity_data_bit_length+1, parity_input_data_decimal_with_parity_bit);
            boost::to_string(_bit_set, _str_bit_set);
            strcpy(parity_input_data_with_parity_bit, _str_bit_set.c_str());
            strcpy(parity_input_data_with_errors_binary, _str_bit_set.c_str());
        }

        ImGui::Text("Data with parity bit:");
        ImGui::InputText("parity binary input with bit", parity_input_data_with_parity_bit, parity_data_bit_length + 2, ImGuiInputTextFlags_ReadOnly);

        ImGui::Text("Data with errors:");
        ImGui::InputText("parity binary input with bit and errors", parity_input_data_with_errors_binary, parity_data_bit_length + 2, ImGuiInputTextFlags_CallbackCharFilter, TextFilters::BinaryNumbersFilter);       
        if (ImGui::Button("Clear errors")) parity_clear_errors++;
        if (parity_clear_errors & 1)
        {
            parity_clear_errors = 0;
            parity_result_color = ImVec4(1.f, 1.f, 0.f, 1.0f);
            strcpy(parity_input_data_with_errors_binary, parity_input_data_with_parity_bit);
        }
        ImGui::Text("Received data:");
        ImGui::TextColored(parity_result_color, parity_input_data_with_errors_binary);
        if (ImGui::Button("validate data")) parity_validate_data++;
        if (parity_validate_data & 1)
        {
            parity_validate_data = 0;
            if (even_or_odd) {
                if (__check_if_even(std::stol(parity_input_data_with_errors_binary, nullptr, 2))) parity_result_color = ImVec4(0.f, 1.f, 0.f, 1.0f);
                else parity_result_color = ImVec4(1.0f, 0.f, 0.f, 1.0f);
            }
            else {
                if (__check_if_odd(std::stol(parity_input_data_with_errors_binary, nullptr, 2))) parity_result_color = ImVec4(0.f, 1.f, 0.f, 1.0f);
                else parity_result_color = ImVec4(1.0f, 0.f, 0.f, 1.0f);
            }
            strcpy(parity_input_data_with_errors_binary, parity_input_data_with_parity_bit);
        }

    }
}
unsigned long MyApp::__make_odd_parity(const unsigned long &data) {
    return __count_ones_parity(data) % 2ul == 1ul ? data << 1ul : (data << 1ul) + 1ul;
}
unsigned long MyApp::__make_even_parity(const unsigned long &data) {
    return __count_ones_parity(data) % 2ul == 1ul ? (data << 1ul) + 1ul : data << 1ul;
}
bool MyApp::__check_if_even(const unsigned long& data) {
    return __count_ones_parity(data) % 2ul == 0ul;
}
bool MyApp::__check_if_odd(const unsigned long& data) {
    return __count_ones_parity(data) % 2ul == 1ul;
}
unsigned MyApp::__count_ones_parity(const unsigned long &data) {
    unsigned long _d = data;
    unsigned counter = 0u;
    do if (_d & 1ul) ++counter;
    while ((_d >>=1ul) > 0);
    return counter;
}
/*PARITY END*/
/*=====================================================================================*/

/*=====================================================================================*/
/*HAMMING BEGIN*/
void MyApp::__hamming_header() {

    if (ImGui::CollapsingHeader("Hamming Coding"))
    {

    }
}
/*HAMMING END*/
/*=====================================================================================*/


/*=====================================================================================*/
/*CRC BEGIN*/
unsigned long MyApp::crc_data_bit_length = 0;
char MyApp::crc_input_data_binary[33];
unsigned long MyApp::crc_input_data_decimal = 0;
char MyApp::crc_input_data_with_errors_binary[33];
unsigned MyApp::crc_input_data_with_errors_decimal = 0;

char MyApp::crc12_hex[10];
char MyApp::crc12_dec[10];
char MyApp::crc12_divisor_bin[CRC12_KEY_BIT_LEN + 1];
unsigned long MyApp::crc12_remainder_dec = 0;
char MyApp::crc12_remainder_bin[CRC12_KEY_BIT_LEN + 1];
char MyApp::crc12_check_bin[CRC12_KEY_BIT_LEN + 1];

char MyApp::crc16_hex[10];
char MyApp::crc16_dec[10];
char MyApp::crc16_divisor_bin[CRC16_KEY_BIT_LEN + 1];
unsigned long MyApp::crc16_remainder_dec = 0;
char MyApp::crc16_remainder_bin[CRC16_KEY_BIT_LEN + 1];
char MyApp::crc16_check_bin[CRC16_KEY_BIT_LEN + 1];

char MyApp::crc16r_hex[10];
char MyApp::crc16r_dec[10];
char MyApp::crc16r_divisor_bin[CRC16R_KEY_BIT_LEN + 1];
unsigned long MyApp::crc16r_remainder_dec = 0;
char MyApp::crc16r_remainder_bin[CRC16R_KEY_BIT_LEN + 1];
char MyApp::crc16r_check_bin[CRC16R_KEY_BIT_LEN + 1];

char MyApp::crc32_hex[16];
char MyApp::crc32_dec[16];
char MyApp::crc32_divisor_bin[CRC32_KEY_BIT_LEN + 1];
unsigned long MyApp::crc32_remainder_dec = 0;
char MyApp::crc32_remainder_bin[CRC32_KEY_BIT_LEN + 1];
char MyApp::crc32_check_bin[CRC32_KEY_BIT_LEN + 1];

void MyApp::__crc_header() {

    if (ImGui::CollapsingHeader("CRC"))
    {
        __crc_input_ui("crc_");
        if (ImGui::TreeNode("CRC-12")) {
            __crc_crc12_tree();
            ImGui::TreePop();
            ImGui::Separator();
        }
        if (ImGui::TreeNode("CRC-16")) {
            __crc_crc16_tree();
            ImGui::TreePop();
            ImGui::Separator();
        }
        if (ImGui::TreeNode("CRC-16 REVERSED")) {
            __crc_crc16r_tree();
            ImGui::TreePop();
            ImGui::Separator();
        }
        if (ImGui::TreeNode("CRC-32")) {
            __crc_crc32_tree();
            ImGui::TreePop();
            ImGui::Separator();
        }
    }
}
void MyApp::__crc_input_ui(std::string labels_prefix) {

    ImGui::Text("Choose mode:");
    static int binary_or_decimal = 1;
    ImGui::RadioButton("binary", &binary_or_decimal, 0); ImGui::SameLine();
    ImGui::RadioButton("decimal", &binary_or_decimal, 1);
    static int crc_generate_data_bits_button_click_counter = 0;
    static int crc_refresh_data_bits_button_click_counter = 0;
    static int crc_refresh_error_bits_button_click_counter = 0;

    ImGui::Text("Input entry data:");

    ImGui::SliderInt("Data bit length", reinterpret_cast<int*>(&crc_data_bit_length), 9,32);


    if (binary_or_decimal) ImGui::InputText("binary_input", crc_input_data_binary, crc_data_bit_length+1, ImGuiInputTextFlags_ReadOnly, TextFilters::BinaryNumbersFilter);
    else ImGui::InputText("binary_input", crc_input_data_binary, crc_data_bit_length+1, ImGuiInputTextFlags_CallbackCharFilter, TextFilters::BinaryNumbersFilter);

    ImGui::SameLine();
    if (ImGui::Button("Generate data bits")) crc_generate_data_bits_button_click_counter++;
    if (crc_generate_data_bits_button_click_counter & 1)
    {
        crc_generate_data_bits_button_click_counter = 0;
        crc_input_data_decimal = __generate_random_number(0, 1ull<<crc_data_bit_length);
        std::string _str_bit_set;
        boost::dynamic_bitset<> _bit_set(crc_data_bit_length, crc_input_data_decimal);
        boost::to_string(_bit_set, _str_bit_set);
        strcpy(crc_input_data_binary, _str_bit_set.c_str());
        strcpy(crc_input_data_with_errors_binary, crc_input_data_binary);
        crc_input_data_with_errors_decimal = crc_input_data_decimal;
    }

    if (binary_or_decimal) ImGui::InputInt("crc_decimal_input", reinterpret_cast<int*>(&crc_input_data_decimal));
    else ImGui::InputInt("crc_decimal_input", reinterpret_cast<int*>(&crc_input_data_decimal), 0, 0, ImGuiInputTextFlags_ReadOnly);

    ImGui::SameLine();
    if (ImGui::Button("Ref. input")) crc_refresh_data_bits_button_click_counter++;
    if (crc_refresh_data_bits_button_click_counter & 1)
    {
        crc_refresh_data_bits_button_click_counter = 0;

        if (binary_or_decimal) {
            std::string _str_bit_set;
            boost::dynamic_bitset<> _bit_set(crc_data_bit_length, crc_input_data_decimal);
            boost::to_string(_bit_set, _str_bit_set);
            strcpy(crc_input_data_binary, _str_bit_set.c_str());
        }
        else crc_input_data_decimal = std::stol(crc_input_data_binary, 0, 2);
    }

    ImGui::Text("Data with errors:");

    if (binary_or_decimal) ImGui::InputText("crc_binary_error_input", crc_input_data_with_errors_binary, crc_data_bit_length+1, ImGuiInputTextFlags_ReadOnly, TextFilters::BinaryNumbersFilter);
    else ImGui::InputText("crc_error_input", crc_input_data_with_errors_binary, crc_data_bit_length+1, ImGuiInputTextFlags_CallbackCharFilter, TextFilters::BinaryNumbersFilter);

    if (binary_or_decimal) ImGui::InputInt("crc_decimal_error_input", reinterpret_cast<int*>(&crc_input_data_with_errors_decimal));
    else ImGui::InputInt("crc_decimal_input", reinterpret_cast<int*>(&crc_input_data_with_errors_decimal), 0, 0, ImGuiInputTextFlags_ReadOnly);
    ImGui::SameLine();
    if (ImGui::Button("Ref. error")) crc_refresh_error_bits_button_click_counter++;
    if (crc_refresh_error_bits_button_click_counter & 1)
    {
        crc_refresh_error_bits_button_click_counter = 0;
        if (binary_or_decimal) {
            std::string _str_bit_set;
            boost::dynamic_bitset<> _bit_set(crc_data_bit_length, crc_input_data_with_errors_decimal);
            boost::to_string(_bit_set, _str_bit_set);
            strcpy(crc_input_data_with_errors_binary, _str_bit_set.c_str());
        }
        else crc_input_data_with_errors_decimal = std::stol(crc_input_data_with_errors_binary, 0, 2);
    }

    if (ImGui::Button("Clear errors")) crc_refresh_data_bits_button_click_counter++;
    if (crc_refresh_data_bits_button_click_counter & 1)
    {

        crc_refresh_data_bits_button_click_counter = 0;
        strcpy(crc_input_data_with_errors_binary, crc_input_data_binary);
        crc_input_data_with_errors_decimal = crc_input_data_decimal;
    }
}
void MyApp::__crc_crc12_tree() {
    static int crc_calc_crc12 = 0;
    static ImVec4 crc12_result_color(1.0f, 1.0f, 0.0f, 1.0f);
    ImGui::Text("CRC12 polynomial:");
    {
        sprintf(crc12_hex, "0x%X\n", CRC12_POLYNOMIAL);
    }
    ImGui::InputText("crc12 pol hex", crc12_hex, sizeof(crc12_hex), ImGuiInputTextFlags_ReadOnly);
    ImGui::Text("CRC12 divisor:");
    {
        std::string _str_bit_set;
        boost::dynamic_bitset<> _bit_set(CRC12_KEY_BIT_LEN+1, (1 << CRC12_KEY_BIT_LEN) + CRC12_POLYNOMIAL);
        boost::to_string(_bit_set, _str_bit_set);
        strcpy(crc12_divisor_bin, _str_bit_set.c_str());
    }
    ImGui::InputText("crc12 divisor bin", crc12_divisor_bin, sizeof(crc12_divisor_bin), ImGuiInputTextFlags_ReadOnly);
    ImGui::Text("CRC12 calculated remainder:");
    ImGui::InputText("crc12 key bin", crc12_remainder_bin, sizeof(crc12_remainder_bin), ImGuiInputTextFlags_ReadOnly);
    ImGui::SameLine();
    if (ImGui::Button("CRC12 remainder bin")) crc_calc_crc12++;
    if (crc_calc_crc12 & 1)
    {
        crc_calc_crc12 = 0;
        std::string _str_bit_set;
        crc12_remainder_dec = __get_reminder_crc_bit_by_bit(crc_input_data_decimal, CRC12_POLYNOMIAL, CRC12_MASK, CRC12_KEY_BIT_LEN);
        boost::dynamic_bitset<> _bit_set(CRC12_KEY_BIT_LEN, crc12_remainder_dec);
        boost::to_string(_bit_set, _str_bit_set);
        strcpy(crc12_remainder_bin, _str_bit_set.c_str());
    }
    ImGui::Text("CRC12 remainder check:");
    ImGui::TextColored(crc12_result_color, crc12_check_bin);
    if (ImGui::Button("CRC12 validate")) crc_calc_crc12++;
    if (crc_calc_crc12 & 1)
    {
        crc_calc_crc12 = 0;
        std::string _str_bit_set;
        unsigned long result = __check_reminder_crc_bit_by_bit(crc_input_data_with_errors_decimal, crc12_remainder_dec, CRC12_POLYNOMIAL, CRC12_MASK, CRC12_KEY_BIT_LEN);
        if (result == 0ul) crc12_result_color = ImVec4(0.f, 1.f, 0.f, 1.0f);
        else crc12_result_color = ImVec4(1.0f, 0.f, 0.f, 1.0f);
        boost::dynamic_bitset<> _bit_set(CRC12_KEY_BIT_LEN, result);
        boost::to_string(_bit_set, _str_bit_set);
        strcpy(crc12_check_bin, _str_bit_set.c_str());
    }
}
void MyApp::__crc_crc16_tree() {
    static int crc_calc_crc16 = 0;
    static ImVec4 crc16_result_color(1.0f, 1.0f, 0.0f, 1.0f);
    ImGui::Text("CRC16 polynomial:");
    {
        sprintf(crc16_hex, "0x%X\n", CRC16_POLYNOMIAL);
    }
    ImGui::InputText("crc16 pol hex", crc16_hex, sizeof(crc16_hex), ImGuiInputTextFlags_ReadOnly);
    ImGui::Text("CRC16 divisor:");
    {
        std::string _str_bit_set;
        boost::dynamic_bitset<> _bit_set(CRC16_KEY_BIT_LEN+1, (1 << CRC16_KEY_BIT_LEN) + CRC16_POLYNOMIAL);
        boost::to_string(_bit_set, _str_bit_set);
        strcpy(crc16_divisor_bin, _str_bit_set.c_str());
    }
    ImGui::InputText("crc16 divisor bin", crc16_divisor_bin, sizeof(crc16_divisor_bin), ImGuiInputTextFlags_ReadOnly);
    ImGui::Text("CRC16 calculated remainder:");
    ImGui::InputText("crc16 key bin", crc16_remainder_bin, sizeof(crc16_remainder_bin), ImGuiInputTextFlags_ReadOnly);
    ImGui::SameLine();
    if (ImGui::Button("CRC16 remainder bin")) crc_calc_crc16++;
    if (crc_calc_crc16 & 1)
    {
        crc_calc_crc16 = 0;
        std::string _str_bit_set;
        crc16_remainder_dec = __get_reminder_crc_bit_by_bit(crc_input_data_decimal, CRC16_POLYNOMIAL, CRC16_MASK, CRC16_KEY_BIT_LEN);
        boost::dynamic_bitset<> _bit_set(CRC16_KEY_BIT_LEN, crc16_remainder_dec);
        boost::to_string(_bit_set, _str_bit_set);
        strcpy(crc16_remainder_bin, _str_bit_set.c_str());
    }
    ImGui::Text("CRC16 remainder check:");
    ImGui::TextColored(crc16_result_color, crc16_check_bin);
    if (ImGui::Button("CRC16 validate")) crc_calc_crc16++;
    if (crc_calc_crc16 & 1)
    {
        crc_calc_crc16 = 0;
        std::string _str_bit_set;
        unsigned long result = __check_reminder_crc_bit_by_bit(crc_input_data_with_errors_decimal, crc16_remainder_dec, CRC16_POLYNOMIAL, CRC16_MASK, CRC16_KEY_BIT_LEN);
        if (result == 0ul) crc16_result_color = ImVec4(0.f, 1.f, 0.f, 1.0f);
        else crc16_result_color = ImVec4(1.0f, 0.f, 0.f, 1.0f);
        boost::dynamic_bitset<> _bit_set(CRC16_KEY_BIT_LEN, result);
        boost::to_string(_bit_set, _str_bit_set);
        strcpy(crc16_check_bin, _str_bit_set.c_str());
    }
}
void MyApp::__crc_crc16r_tree() {
    static int crc_calc_crc16r = 0;
    static ImVec4 crc16r_result_color(1.0f, 1.0f, 0.0f, 1.0f);
    ImGui::Text("CRC16R polynomial:");
    {
        sprintf(crc16r_hex, "0x%X\n", CRC16R_POLYNOMIAL);
    }
    ImGui::InputText("crc16r pol hex", crc16r_hex, sizeof(crc16r_hex), ImGuiInputTextFlags_ReadOnly);
    ImGui::Text("CRC16R divisor:");
    {
        std::string _str_bit_set;
        boost::dynamic_bitset<> _bit_set(CRC16R_KEY_BIT_LEN+1, (1 << CRC16R_KEY_BIT_LEN) + CRC16R_POLYNOMIAL);
        boost::to_string(_bit_set, _str_bit_set);
        strcpy(crc16r_divisor_bin, _str_bit_set.c_str());
    }
    ImGui::InputText("crc16r divisor bin", crc16r_divisor_bin, sizeof(crc16r_divisor_bin), ImGuiInputTextFlags_ReadOnly);
    ImGui::Text("CRC16R calculated remainder:");
    ImGui::InputText("crc16r key bin", crc16r_remainder_bin, sizeof(crc16r_remainder_bin), ImGuiInputTextFlags_ReadOnly);
    ImGui::SameLine();
    if (ImGui::Button("CRC16R remainder bin")) crc_calc_crc16r++;
    if (crc_calc_crc16r & 1)
    {
        crc_calc_crc16r = 0;
        std::string _str_bit_set;
        crc16r_remainder_dec = __get_reminder_crc_bit_by_bit(crc_input_data_decimal, CRC16R_POLYNOMIAL, CRC16R_MASK, CRC16R_KEY_BIT_LEN);
        boost::dynamic_bitset<> _bit_set(CRC16R_KEY_BIT_LEN, crc16r_remainder_dec);
        boost::to_string(_bit_set, _str_bit_set);
        strcpy(crc16r_remainder_bin, _str_bit_set.c_str());
    }
    ImGui::Text("CRC16R remainder check:");
    ImGui::TextColored(crc16r_result_color, crc16r_check_bin);
    if (ImGui::Button("CRC16R validate")) crc_calc_crc16r++;
    if (crc_calc_crc16r & 1)
    {
        crc_calc_crc16r = 0;
        std::string _str_bit_set;
        unsigned long result = __check_reminder_crc_bit_by_bit(crc_input_data_with_errors_decimal, crc16r_remainder_dec, CRC16R_POLYNOMIAL, CRC16R_MASK, CRC16R_KEY_BIT_LEN);
        if (result == 0ul) crc16r_result_color = ImVec4(0.f, 1.f, 0.f, 1.0f);
        else crc16r_result_color = ImVec4(1.0f, 0.f, 0.f, 1.0f);
        boost::dynamic_bitset<> _bit_set(CRC16R_KEY_BIT_LEN, result);
        boost::to_string(_bit_set, _str_bit_set);
        strcpy(crc16r_check_bin, _str_bit_set.c_str());
    }
}
void MyApp::__crc_crc32_tree() {
    static int crc_calc_crc32 = 0;
    static ImVec4 crc32_result_color(1.0f, 1.0f, 0.0f, 1.0f);
    ImGui::Text("CRC32 polynomial:");
    {
        sprintf(crc32_hex, "0x%X\n", CRC32_POLYNOMIAL);
    }
    ImGui::InputText("crc32 pol hex", crc32_hex, sizeof(crc32_hex), ImGuiInputTextFlags_ReadOnly);
    ImGui::Text("CRC32 divisor:");
    {
        std::string _str_bit_set;
        boost::dynamic_bitset<> _bit_set(CRC32_KEY_BIT_LEN+1, (1 << CRC32_KEY_BIT_LEN) + CRC32_POLYNOMIAL);
        boost::to_string(_bit_set, _str_bit_set);
        strcpy(crc32_divisor_bin, _str_bit_set.c_str());
    }
    ImGui::InputText("crc32 divisor bin", crc32_divisor_bin, sizeof(crc32_divisor_bin), ImGuiInputTextFlags_ReadOnly);
    ImGui::Text("CRC32 calculated remainder:");
    ImGui::InputText("crc32 key bin", crc32_remainder_bin, sizeof(crc32_remainder_bin), ImGuiInputTextFlags_ReadOnly);
    ImGui::SameLine();
    if (ImGui::Button("CRC32 remainder bin")) crc_calc_crc32++;
    if (crc_calc_crc32 & 1)
    {
        crc_calc_crc32 = 0;
        std::string _str_bit_set;
        crc32_remainder_dec = __get_reminder_crc_bit_by_bit(crc_input_data_decimal, CRC32_POLYNOMIAL, CRC32_MASK, CRC32_KEY_BIT_LEN);
        boost::dynamic_bitset<> _bit_set(CRC32_KEY_BIT_LEN, crc32_remainder_dec);
        boost::to_string(_bit_set, _str_bit_set);
        strcpy(crc32_remainder_bin, _str_bit_set.c_str());
    }
    ImGui::Text("CRC32 remainder check:");
    ImGui::TextColored(crc32_result_color, crc32_check_bin);
    if (ImGui::Button("CRC32 validate")) crc_calc_crc32++;
    if (crc_calc_crc32 & 1)
    {
        crc_calc_crc32 = 0;
        std::string _str_bit_set;
        unsigned long result = __check_reminder_crc_bit_by_bit(crc_input_data_with_errors_decimal, crc32_remainder_dec, CRC32_POLYNOMIAL, CRC32_MASK, CRC32_KEY_BIT_LEN);
        if (result == 0ul) crc32_result_color = ImVec4(0.f, 1.f, 0.f, 1.0f);
        else crc32_result_color = ImVec4(1.0f, 0.f, 0.f, 1.0f);
        boost::dynamic_bitset<> _bit_set(CRC32_KEY_BIT_LEN, result);
        boost::to_string(_bit_set, _str_bit_set);
        strcpy(crc32_check_bin, _str_bit_set.c_str());
    }
}
unsigned long MyApp::__get_reminder_crc_bit_by_bit(unsigned long data, long polynomial, long mask, unsigned key_len) {
    //to kurwa działa zajebiście
    std::cout << "Data: " << data << std::endl;
    std::bitset<64> star(data);
    std::cout << "Data: " << star << std::endl;

    unsigned long long tmp = 1ull;
    int start_off = 0;
    while (data > (tmp <<= 1)) ++start_off;
    unsigned long long head = 1ull << (start_off + key_len);
    unsigned long long mask_subset = ((mask << 1ull) + 1ull) << start_off;
    unsigned long long mask_crc_8_dividor = ((1ull << key_len) + polynomial) << start_off;
    unsigned long long result = data;
    result <<= key_len;


    // std::bitset<64> h(head);
    // std::cout << "H:" << h << std::endl;
    // std::bitset<64> dupa(result);
    // std::cout << "B:" << dupa << std::endl;
    // std::bitset<64> z(mask_crc_8_dividor);
    // std::cout << "A:" << z << std::endl << std::endl;
    // std::bitset<64> y(mask_subset);
    // std::cout << "D:" << y << std::endl;
    
    while ((result & (~mask)) > 0ull) {
        result = (result & (~mask_subset)) | ((result & mask_subset) ^ mask_crc_8_dividor);
        while (head > result) {
            head >>= 1ull;
            mask_subset >>= 1ull;
            mask_crc_8_dividor >>= 1ull;
        }
    }
    return result;
}
unsigned long MyApp::__check_reminder_crc_bit_by_bit(unsigned long data, long reminder, long polynomial, long mask, unsigned key_len) {
    //to też kurwa działa zajebiście
    unsigned long long tmp = 1ull;
    int start_off = 0;
    std::cout << data << std::endl;
    while (data > (tmp <<= 1)) ++start_off;
    unsigned long long head = 1ull << (start_off + key_len);
    unsigned long long mask_subset = ((mask << 1ull) + 1ull) << start_off;
    unsigned long long mask_crc_8_dividor = ((1ull << key_len) + polynomial) << start_off;
    unsigned long long result = data;
    result <<= key_len;
    result |= reminder;

    // std::bitset<64> h(head);
    // std::cout << "H:" << h << std::endl;
    // std::bitset<64> dupa(result);
    // std::cout << "B:" << dupa << std::endl;
    // std::bitset<64> z(mask_crc_8_dividor);
    // std::cout << "A:" << z << std::endl << std::endl;
    // std::bitset<64> y(mask_subset);
    // std::cout << "D:" << y << std::endl;
    
    while ((result & (~mask)) > 0ull) {
        result = (result & (~mask_subset)) | ((result & mask_subset) ^ mask_crc_8_dividor);
        while (head > result) {
            head >>= 1ull;
            mask_subset >>= 1ull;
            mask_crc_8_dividor >>= 1ull;
        }
    }
    return result;
}
/*CRC END*/
/*=====================================================================================*/

MyApp::MyApp() {
    generator = std::mt19937(device());
    distribution.param(std::uniform_int<unsigned long long>::param_type(0, ULONG_MAX));
}

void MyApp::ui() {
    ImGui::Begin("Parity, Hamming and CRC");
    __parity_header();
    if (ImGui::CollapsingHeader("Hamming"))
    {
        ImGui::Text("ABOUT THIS DEMO:");
    }
    __crc_header();
    ImGui::End();
}

unsigned long long MyApp::__generate_random_number(size_t const &min, unsigned long long const &max) {
    distribution.param(std::uniform_int<unsigned long long>::param_type(min, max));
    return distribution(generator);
}
